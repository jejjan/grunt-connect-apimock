/*
 * grunt-connect-apimock
 * git-ljo
 *
 * Copyright (c) 2015 Lars Johansson
 * Licensed under the MIT license.
 */

'use strict';

var util = require('util');
var apimock = require('../lib/apimock');

module.exports = function(grunt) {

    // Please see the Grunt documentation for more information regarding task
    // creation: http://gruntjs.com/creating-tasks

    grunt.registerTask('configureApimock', 'Configure apimock.', function(target) {
        var configLocation = 'connect.apimock';
        if(target) {
            configLocation = 'connect.'+target+'.apimock';
        }

        var apimockOptions = grunt.config(configLocation);
        if(apimockOptions === undefined) {
            grunt.log.error(configLocation + ' configuration is missing!');
            return;
        }

        var optionsArray = util.isArray(apimockOptions) ? apimockOptions : [apimockOptions];
        optionsArray.forEach(function(option, index) {
            if(option.url === undefined) {
                grunt.fail.fatal('url configuration is missing for option ' + index);
            }
            if (option.dir === undefined) {
                grunt.fail.fatal('dir configuration is missing for option ' + index);
            }
            if(option.delay !== undefined && !isNumber(option.delay)) {
                grunt.fail.fatal('delay configuration is not a number: ' + option.delay);
            }
        });

        apimock.config(apimockOptions);
    });

};

function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}