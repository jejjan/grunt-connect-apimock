/*
 * grunt-connect-apimock
 *
 * Copyright (c) 2015 Lars Johansson
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function(grunt) {
    // Actually load this plugin's task(s).
    grunt.loadTasks('tasks');

    // These plugins provide necessary tasks.
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-eslint');
    grunt.loadNpmTasks('grunt-mocha-test');

    //Main tests task
    //Test common apimock configuration
    grunt.registerTask('test', [
        'configureApimock',
        'connect:commontest',
        'mochaTest:commontest'
    ]);

    //Test connect target apimock configuration
    grunt.registerTask('targetconfigtest', [
        'configureApimock:targetconfigtest',
        'connect:targetconfigtest',
        'mochaTest:targetconfigtest',
    ]);

    grunt.registerTask('serve', ['configureApimock', 'connect:serve', 'watch']);

    // By default, lint and run all tests.
    grunt.registerTask('default', ['eslint', 'test']);

    // Project configuration.
    grunt.initConfig({

        eslint: {
            target: [
                'Gruntfile.js',
                'tasks/*.js',
                'lib/*.js',
                'test/**/*.js'
            ]
        },

        // Unit tests.
        mochaTest: {
            options: {
                reporter: 'spec',
                quiet: false,
                clearRequireCache: false
            },
            commontest: {
                src: ['test/commontest/**/*.spec.js']
            },
            targetconfigtest: {
                src: ['test/targetconfigtest/**/*.spec.js']
            }
        },

        //web server for testing the plugin
        connect: {
            options: {
                port: 8000,
                // Change this to '0.0.0.0' to access the server from outside.
                hostname: '0.0.0.0',//'localhost',
                livereload: 35729,
                middleware: function(/*connect*/) {
                    var middlewares = [];
                    //mock rest api with files
                    middlewares.push(require('./lib/apimock').mockRequest);
                    return middlewares;
                }
            },
            apimock: {
                //common configuration
                url: '/api/',
                dir: 'test/api'
            },
            serve: {
                options:{
                }
            },
            commontest: {
                options:{
                    port: 8080
                }
            },
            targetconfigtest: {
                apimock: [
                    //target configuration
                    {url: '/api2/', dir: 'test/api'},
                    {url: '/apiX/', dir: 'test/apiX', delay: 1000}
                ],
                options:{
                    port: 8080
                }
            }
        },

        //watch
        watch: {
            js: {
                files: ['lib/**/*.js', 'test/**/*.js', 'tasks/**/*.js'],
                tasks: ['eslint', 'test']
            }
        }

    });
};
