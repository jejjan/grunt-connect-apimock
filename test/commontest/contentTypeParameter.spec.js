var chai = require('chai');
var expect = chai.expect;
var request = require('request');

describe('Cookies', function() {

    it('Should return the response for the first cookie match', function(done) {
        request.get('http://localhost:8080/api/advanced/text_plain', function(err, res, body) {
            expect(res.statusCode).to.equal(200);
            expect(res.headers['content-type']).to.equal('text/plain');
            expect(body).to.equal('This is a text');
            done();
        });
    });
});
